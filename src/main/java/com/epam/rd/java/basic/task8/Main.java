package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.*;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {

	public static Flower buildFlower(Element flowerElement) {
		Flower flower = new Flower();
		flower.setName(getElementTextContent(flowerElement, "name"));
		flower.setSoil(getElementTextContent(flowerElement, "soil"));
		flower.setOrigin(getElementTextContent(flowerElement, "origin"));
		flower.setStemColour(getElementTextContent(flowerElement, "stemColour"));
		flower.setLeafColour(getElementTextContent(flowerElement, "leafColour"));
		flower.setAveLenFlower(getElementTextContent(flowerElement, "aveLenFlower"));
		flower.setTemperature(getElementTextContent(flowerElement, "tempreture"));
		flower.setLighting(getElementTextContent(flowerElement, "lighting"));
		flower.setWatering(getElementTextContent(flowerElement, "watering"));
		flower.setMultiplying(getElementTextContent(flowerElement, "multiplying"));
		return flower;
	}

	private static String getElementTextContent(Element element, String elementName) {
		NodeList nList = element.getElementsByTagName(elementName);
		Node node = nList.item(0);
		String text = node.getTextContent();
		return text;
	}

	private static void write_Xml(Document document, OutputStream output_file)
			throws TransformerException {

		TransformerFactory Transformer_Factory = TransformerFactory.newInstance();
		Transformer New_Transformer = Transformer_Factory.newTransformer();
		DOMSource Source_XML = new DOMSource(document);
		StreamResult Result_XML = new StreamResult(output_file);
		New_Transformer.transform(Source_XML, Result_XML);

	}

	public static void main(String[] args) throws ParserConfigurationException,
			IOException, TransformerException, SAXException, XMLStreamException {


		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(domController.getXmlFileName());
		document.getDocumentElement().normalize();

		NodeList flowerList = document.getElementsByTagName("flower");
		Set<Flower> flowers = new HashSet<>();
		for (int i = 0; i < flowerList.getLength(); i++) {
			Element flowerElement = (Element) flowerList.item(i);
			Flower flower = buildFlower(flowerElement);
			flowers.add(flower);
		}
		System.out.println(flowers.toString());


		// sort (case 1)
		// PLACE YOUR CODE HERE

		// save

		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		DocumentBuilderFactory Doc_Build_Factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder Document_Builder = Doc_Build_Factory.newDocumentBuilder();

		// the root elements for DOM Document
		//create XML fields, elements, etc.
		Document docs = Document_Builder.newDocument();
		Element Root_Element = docs.createElement("School");


		// write DOM document in a file

		try (
				FileOutputStream output_file = new FileOutputStream(outputXmlFile)) {
			write_Xml(document, output_file);
			System.out.println("The XML has been successfully written to school.xml");
		} catch (IOException e) {
			e.printStackTrace();
		}


		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController();
		// PLACE YOUR CODE HERE
		saxController.buildSetFlowers(xmlFileName);
		System.out.println(saxController.getFlowerSet());
		Set<Flower> flowerSet = saxController.getFlowerSet();

		// sort  (case 2)
		// PLACE YOUR CODE HERE

		// save
		String outputSAXFile = "output.sax.xml";
		XMLOutputFactory output = XMLOutputFactory.newInstance();
		XMLEventFactory eventFactory = XMLEventFactory.newInstance();
		XMLStreamWriter writers = output.createXMLStreamWriter(new FileOutputStream(outputSAXFile));

		writers.writeStartDocument("utf-8", "1.0");

		writers.writeStartElement("flowers");
		writers.writeAttribute("xmlns", "http://www.nure.ua" );
		writers.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		writers.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");


		writers.writeCharacters("\n");
		for (Flower flowery:flowerSet) {
			writers.writeCharacters("\n\t");
			writers.writeStartElement("flower");

			writers.writeCharacters("\n\t\t");
			writers.writeStartElement("name");
			writers.writeCharacters(flowery.getName());
			writers.writeEndElement();

			writers.writeCharacters("\n\t\t");
			writers.writeStartElement("soil");
			writers.writeCharacters(flowery.getSoil());
			writers.writeEndElement();

			writers.writeCharacters("\n\t\t");
			writers.writeStartElement("origin");
			writers.writeCharacters(flowery.getOrigin());
			writers.writeEndElement();

			writers.writeCharacters("\n\t\t");
			writers.writeStartElement("visualParameters");


			writers.writeCharacters("\n\t\t\t");
			writers.writeStartElement("stemColour");
			writers.writeCharacters(flowery.getStemColour());
			writers.writeEndElement();

			writers.writeCharacters("\n\t\t\t");
			writers.writeStartElement("leafColour");
			writers.writeCharacters(flowery.getLeafColour());
			writers.writeEndElement();

			writers.writeCharacters("\n\t\t\t");
			writers.writeStartElement("aveLenFlower");
			writers.writeAttribute("measure", "cm");
			writers.writeCharacters(flowery.getAveLenFlower());
			writers.writeEndElement();

			writers.writeCharacters("\n\t\t");
			writers.writeEndElement();

			writers.writeCharacters("\n\t\t");
			writers.writeStartElement("growingTips");

			writers.writeCharacters("\n\t\t\t");
			writers.writeStartElement("tempreture");
			writers.writeAttribute("measure", "celcius");
			writers.writeCharacters(flowery.getTemperature());
			writers.writeEndElement();

			writers.writeCharacters("\n\t\t\t");
			writers.writeStartElement("lighting");
			writers.writeAttribute("lightRequiring", "yes");
			writers.writeEndElement();

			writers.writeCharacters("\n\t\t\t");
			writers.writeStartElement("watering");
			writers.writeAttribute("measure", "mlPerWeek");
			writers.writeCharacters(flowery.getWatering());
			writers.writeEndElement();

			writers.writeCharacters("\n\t\t");
			writers.writeEndElement();

			writers.writeCharacters("\n\t\t");
			writers.writeStartElement("multiplying");
			writers.writeCharacters(flowery.getMultiplying());
			writers.writeEndElement();

			writers.writeCharacters("\n\t");
			writers.writeEndElement();
		}
		writers.writeEndElement();
		writers.writeEndDocument();
		writers.flush();

		writers.close();

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		XMLEventReader eventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));

		List<Flower> listFlowers = new ArrayList<>();
		Flower flower = null;
		while (eventReader.hasNext()){
			XMLEvent event = eventReader.nextEvent();
			if (event.isStartElement()) {
				StartElement startElement = event.asStartElement();
				if("flower".equalsIgnoreCase(startElement.getName().getLocalPart())){
					flower = new Flower();
				}
				switch (startElement.getName().getLocalPart()){
					case "name":
						event = eventReader.nextEvent();
						flower.setName(event.asCharacters().getData());
						break;
					case "soil":
						event = eventReader.nextEvent();
						flower.setSoil(event.asCharacters().getData());
						break;
					case "origin":
						event = eventReader.nextEvent();
						flower.setOrigin(event.asCharacters().getData());
						break;
					case "stemColour":
						event = eventReader.nextEvent();
						flower.setStemColour(event.asCharacters().getData());
						break;
					case "leafColour":
						event = eventReader.nextEvent();
						flower.setLeafColour(event.asCharacters().getData());
						break;
					case "aveLenFlower":
						event = eventReader.nextEvent();
						flower.setAveLenFlower(event.asCharacters().getData());
						break;
					case "tempreture":
						event = eventReader.nextEvent();
						flower.setTemperature(event.asCharacters().getData());
						break;
					case"lighting":
						System.out.println("lighting");
//						event = eventReader.nextEvent();
//						String nullLighting = "";
//						if (event.asCharacters().getData().equals(nullLighting)){
//							flower.setLighting("");
//						}
						break;
					case "watering":
						event = eventReader.nextEvent();
						flower.setWatering(event.asCharacters().getData());
						break;
					case "multiplying":
						event = eventReader.nextEvent();
						flower.setMultiplying(event.asCharacters().getData());
						break;
				}

			}
			if (event.isEndElement()) {
				EndElement endElement = event.asEndElement();
				if ("flower".equalsIgnoreCase(endElement.getName().getLocalPart())){
					listFlowers.add(flower);
				}
			}

			// sort  (case 3)
			// PLACE YOUR CODE HERE

			// save
			//	outputXmlFile = "output.stax.xml";
			// PLACE YOUR CODE HERE
		}
		System.out.println("fgoo");
		System.out.println(listFlowers);

		//write to file from stax
		String outputStaxFile = "output.stax.xml";
		XMLStreamWriter writer = output.createXMLStreamWriter(new FileOutputStream(outputStaxFile));
		XMLEvent events = eventFactory.createStartDocument();

		writer.writeStartDocument("utf-8", "1.0");

		writer.writeStartElement("flowers");
		writer.writeAttribute("xmlns", "http://www.nure.ua" );
		writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		writer.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");


		writer.writeCharacters("\n");
		for (Flower flowery:listFlowers) {
			writer.writeCharacters("\n\t");
			writer.writeStartElement("flower");

			writer.writeCharacters("\n\t\t");
			writer.writeStartElement("name");
			writer.writeCharacters(flowery.getName());
			writer.writeEndElement();

			writer.writeCharacters("\n\t\t");
			writer.writeStartElement("soil");
			writer.writeCharacters(flowery.getSoil());
			writer.writeEndElement();

			writer.writeCharacters("\n\t\t");
			writer.writeStartElement("origin");
			writer.writeCharacters(flowery.getOrigin());
			writer.writeEndElement();

			writer.writeCharacters("\n\t\t");
			writer.writeStartElement("visualParameters");


			writer.writeCharacters("\n\t\t\t");
			writer.writeStartElement("stemColour");
			writer.writeCharacters(flowery.getStemColour());
			writer.writeEndElement();

			writer.writeCharacters("\n\t\t\t");
			writer.writeStartElement("leafColour");
			writer.writeCharacters(flowery.getLeafColour());
			writer.writeEndElement();

			writer.writeCharacters("\n\t\t\t");
			writer.writeStartElement("aveLenFlower");
			writer.writeAttribute("measure", "cm");
			writer.writeCharacters(flowery.getAveLenFlower());
			writer.writeEndElement();

			writer.writeCharacters("\n\t\t");
			writer.writeEndElement();

			writer.writeCharacters("\n\t\t");
			writer.writeStartElement("growingTips");

			writer.writeCharacters("\n\t\t\t");
			writer.writeStartElement("tempreture");
			writer.writeAttribute("measure", "celcius");
			writer.writeCharacters(flowery.getTemperature());
			writer.writeEndElement();

			writer.writeCharacters("\n\t\t\t");
			writer.writeStartElement("lighting");
			writer.writeAttribute("lightRequiring", "yes");
			writer.writeEndElement();

			writer.writeCharacters("\n\t\t\t");
			writer.writeStartElement("watering");
			writer.writeAttribute("measure", "mlPerWeek");
			writer.writeCharacters(flowery.getWatering());
			writer.writeEndElement();

			writer.writeCharacters("\n\t\t");
			writer.writeEndElement();

			writer.writeCharacters("\n\t\t");
			writer.writeStartElement("multiplying");
			writer.writeCharacters(flowery.getMultiplying());
			writer.writeEndElement();

			writer.writeCharacters("\n\t");
			writer.writeEndElement();
		}
		writer.writeEndElement();
		writer.writeEndDocument();
		writer.flush();

		writer.close();




	}

}
