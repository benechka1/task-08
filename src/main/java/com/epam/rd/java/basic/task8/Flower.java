package com.epam.rd.java.basic.task8;

import javax.xml.namespace.QName;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private String visualParameters;
    private String stemColour;
    private String leafColour;
    private String aveLenFlower;
    private String growingTips;
    private String temperature;
    private String lighting;
    private String watering;
    private String multiplying;

    public Flower() {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
        this.temperature = temperature;
        this.lighting = lighting;
        this.watering = watering;
        this.multiplying = multiplying;
    }

    public String getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(String growingTips) {
        this.growingTips = growingTips;
    }

    public String getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(String visualParameters) {
        this.visualParameters = visualParameters;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower='" + aveLenFlower + '\'' +
                ", temperature='" + temperature + '\'' +
                ", lighting='" + lighting + '\'' +
                ", watering='" + watering + '\'' +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public String getAveLenFlower() {
        return aveLenFlower;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getLighting() {
        return lighting;
    }

    public String getWatering() {
        return watering;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setAveLenFlower(String aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public void setWatering(String watering) {
        this.watering = watering;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }
}