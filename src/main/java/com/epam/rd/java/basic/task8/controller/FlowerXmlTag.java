package com.epam.rd.java.basic.task8.controller;

public enum FlowerXmlTag {
    FLOWERS("flowers"),
    NAME("name"),
    SOIL("soil"),
    ORIGIN("origin"),
    VISUALPARAMETERS("visualParameters"),
    STEMCOLOUR("stemColour"),
    LEAFCOLOUR("leafColour"),
    AVELENFLOWER("aveLenFlower"),
    GROWINGTIPS("growingTips"),
    TEMPRETURE("tempreture"),
    LIGHTING("lighting"),
    WATERING("watering"),
    MULTIPLYING("multiplying");
    private String value;
    FlowerXmlTag(String value){
        this.value = value;
    }
    public String getValue() {
        return value;
    }

}
