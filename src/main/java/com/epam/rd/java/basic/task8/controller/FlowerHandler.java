package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class FlowerHandler extends DefaultHandler {
    private Set<Flower> flowerSet;
    private Flower current;
    private FlowerXmlTag currentXmlTag;
    private EnumSet<FlowerXmlTag> withText;
    private static final String ELEMENT_FLOWER = "flower";

    public FlowerHandler() {
        flowerSet = new HashSet<>();
        withText = EnumSet.range(FlowerXmlTag.NAME, FlowerXmlTag.MULTIPLYING);
    }
    public Set<Flower> getFlowerSet() {
        return flowerSet;
    }
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (ELEMENT_FLOWER.equals(qName)){
            current = new Flower();
        } else {
            FlowerXmlTag temp = FlowerXmlTag.valueOf(qName.toUpperCase());
            if (withText.contains(temp)){
                currentXmlTag = temp;
            }
        }
    }
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(ELEMENT_FLOWER.equals(qName)){
            flowerSet.add(current);
        }
    }


    @Override
    public void startDocument() throws SAXException {
        System.out.println("Parsing Started");
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("\nParsing ended");
    }



    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String data = new String(ch, start, length).strip();
        if (currentXmlTag != null) {
            switch (currentXmlTag){
                case NAME:
                    current.setName(data);
                    break;
                case SOIL:
                    current.setSoil(data);
                    break;
                case ORIGIN:
                    current.setOrigin(data);
                    break;
                case VISUALPARAMETERS:
                    current.setVisualParameters(data);
                    break;
                case STEMCOLOUR:
                    current.setStemColour(data);
                    break;
                case LEAFCOLOUR:
                    current.setLeafColour(data);
                    break;
                case AVELENFLOWER:
                    current.setAveLenFlower(data);
                    break;
                case GROWINGTIPS:
                    current.setGrowingTips(data);
                    break;
                case TEMPRETURE:
                    current.setTemperature(data);
                    break;
                case LIGHTING:
                    current.setLighting(data);
                    break;
                case WATERING:
                    current.setWatering(data);
                    break;
                case MULTIPLYING:
                    current.setMultiplying(data);
                    break;
                default:
                    throw new EnumConstantNotPresentException(
                        currentXmlTag.getDeclaringClass(), currentXmlTag.name());
                }
        }
        currentXmlTag = null;

    }
}
